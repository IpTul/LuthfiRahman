<!doctype html>
<html lang="en">
	<head>
		<title>:: IPTUL ::</title>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<!--header-->
		<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #202020;">
		<div class="container">
			<a class="navbar-brand" href="#"><img src="imgs/iptul_logo-ss.png"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Service</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Courses</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">About us</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Contact</a>
					</li>
					<button class="btn btn-primary">Log In</button>
				</ul>	
			</div>
		</div>
	</nav>

	<!--jumbotron-->
	<div class="jumbotron jumbotron-fluid d-flex justify-content-center">
		<div class="container">
			<div class="row">
				<div class="col-lg- col-md-8 col-sm-12">
					<h1 class="display-4">Get Accses to Unlimited Eductional Resource, Everywhere, EveryTime!</h1>
					<p class="lead">premium accses to more than 10,000 resources ranging from courses, events e.t.c</p>
					<button class="btn btn-primary">Get Accses</button>				
				</div>	
			</div>
		</div>
	</div>
		
	<!--services-->
	<section class="services d-flex align-items-center justify-content-center" id="services">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 mb-5">
					<h4>services</h4>
				</div>	
			</div>
			<div class="row">
				<div class="col-lg-4">
					<img src="imgs/service1.svg" alt="">
					<h6>Unlimited Accses</h6>
					<p>One subscription unlimited accses</p>
				</div>
				<div class="col-lg-4">
					<img src="imgs/service2.svg" alt="">
					<h6>Expert Teachers</h6>
					<p>Learn from industry experts who are passionate about teaching</p>
				</div>
				<div class="col-lg-4">
					<img src="imgs/service3.svg" alt="">
					<h6>Learn Anywhere</h6>
					<p>Switch between your computer, tablet, or mobile device.</p>
				</div>
			</div>
		</div>
	</section>

	<!--stories-->
	<section class="stories d-flex align-items-center" id="stories">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<h1>Success Stories From Our Students WorldWide!</h1>
					<p>Semaj Africa is an online education platform that delivers video courses, programs and resources for individual, Advertising & Media Specialist Online Marketing Professionals. Freelance and anyone looking to pursue a career in digital marketing. Accounting. Web developments. Programming Multimedia and CAD design</p>
					<button class="btn btn-primary">Discover</button>
				</div>
				<div class="col-lg-8 text-center">
					<img src="imgs/stories-img.png" alt="" width="80%">
				</div>
			</div>
		</div>
	</section>

	<!--Courses-->
	<section class="courses d-flex align-items-center" id="courses">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h4>Courses</h4>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 text-center">
					<a href="">All</a>
					<a href="">Design</a>
					<a href="">Web Development</a>
					<a href="">Digital</a>
					<a href="">Photography</a>
					<a href="">Digital Marketing</a>
				</div>
			</div>
	
			<div class="row pt-4">
				<?php
					include "koneksi.php";
					$data = mysqli_query($koneksi, "select*from courses");
					while($d = mysqli_fetch_array($data)) {
						?>
						<div class="col-lg-3">
							<div class="card">
								<div class="card-top text-center pt-1">
									<?php echo $d['pricing'] ?>
								</div>
								<img src="imgs/<?php echo $d['img'] ?>" class="card-img-top" alt="...">
								<div class="card-body">
									<p class="card-text"><?php echo $d['description'] ?></p>
								</div>
							</div>
						</div>
						<?php
					}
				?>
			</div>

			<div class="row justify-content-center mt-5">
				<button class="btn btn-primary">Discover</button>
			</div>
		</div>
	</section>

	<!-- Achievement -->
	<section class="achievement d-flex align-items-center" id="achievemnt">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<img src="imgs/Achievement1.svg" alt="">
					<h1>5,679</h1>
					<p>Registered Students</p>
				</div>
				<div class="col-lg-3">
					<img src="imgs/Achievement2.svg" alt="">
					<h1>2,679</h1>
					<p>Student has been helped to archive their dreams</p>
				</div>
				<div class="col-lg-3">
					<img src="imgs/Achievement3.svg" alt="">
					<h1>10,000</h1>
					<p>More than 10,000 people visits our site monthly</p>
				</div>
				<div class="col-lg-3">
					<img src="imgs/Achievement4.svg" alt="">
					<h1>#10</h1>
					<p>Ranked among the top 10 growing online learning startups in West Africa</p>
				</div>
			</div>
		</div>
	</section>

	<!-- Testimonials -->

	<section class="testimonials d-flex align-items-center" id="testimonials">
		<div class="container-fluid">
			<div class="row text-center">
				<div class="col-lg-12">
					<h4>What Students Say</h4>
				</div>
			</div>
			<div class="row text-center mb-3 justify-content-center">
				<div class="col-lg-8">
					<p>Semaj Africa is an online education platform that delivers video courses,
						programs and resources for
						individual, Advertising & Media Specialist..
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3">
					<div class="card">
						<div class="card-body">
						<img src="imgs/testi.svg" alt="">
						<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses,
							programs and resources for
							individual, Advertising & Media Specialist. Online Marketing Proffesionals Freelancers and anyone.</p>
							<div class="row">
								<div class="col-lg-3">
									<img src="imgs/User-Pic.svg" alt="">
								</div>
								<div class="col-lg-7 pt-2 ml-2">
									<h5>Alexander Iptul</h5>
									<p>Categories 3D Modelling</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3"><div class="card">
					<div class="card-body">
					<img src="imgs/testi.svg" alt="">
					<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses,
						programs and resources for
						individual, Advertising & Media Specialist. Online Marketing Proffesionals Freelancers and anyone.</p>
						<div class="row">
							<div class="col-lg-3">
								<img src="imgs/User-Pic.svg" alt="">
							</div>
							<div class="col-lg-7 pt-2 ml-2">
								<h5>Shin Ryujin</h5>
								<p>Categories Relationship</p>
							</div>
						</div>
					</div>
				</div></div>
				<div class="col-lg-3"><div class="card">
					<div class="card-body">
					<img src="imgs/testi.svg" alt="">
					<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses,
						programs and resources for
						individual, Advertising & Media Specialist. Online Marketing Proffesionals Freelancers and anyone.</p>
						<div class="row">
							<div class="col-lg-3">
								<img src="imgs/User-Pic.svg" alt="">
							</div>
							<div class="col-lg-7 pt-2 ml-2">
								<h5>Lee Chaeryeong</h5>
								<p>Categories Designer</p>
							</div>
						</div>
					</div>
				</div></div>
				<div class="col-lg-3"><div class="card">
					<div class="card-body">
					<img src="imgs/testi.svg" alt="">
					<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses,
						programs and resources for
						individual, Advertising & Media Specialist. Online Marketing Proffesionals Freelancers and anyone.</p>
						<div class="row">
							<div class="col-lg-3">
								<img src="imgs/User-Pic.svg" alt="">
							</div>
							<div class="col-lg-7 pt-2 ml-2">
								<h5>Alicia Joanne Valerie</h5>
								<p>Categories Social Media Expert</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Newsletter -->
	<section class="newsletter d-flex align-items-center" id="newsletter">
		<div class="container">
			<img src="imgs/Rectangle7.png" alt="" class="imgs">
			<div class="row justify-content-center mt-5">
				<div class="col-lg-8">
					<h4>Subscribe Our Newsletter</h4>
					<p>Get exclusive discounts and latest news deliverd to your inbox for free!</p>
					<button class="btn btn-primary">Start free trial</button>
				</div>
			</div>
		</div>
	</section>

	<!-- Contact -->
	<section class="contact d-flex align-items-center" id="contact">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-3">
					<img src="imgs/iptul_logo-ss.png" alt="" class="mb-3">
					<p>emaj Africa is an online education platform that delivers video courses,
						programs and resources.</p>
					<img src="imgs/sosmed.svg" alt="" class="mt-5">
				</div>
				<div class="col-lg-2">
					<h6>Quicklinks</h6>
					<ul>
						<li>Home</li>
						<li>Courses</li>
						<li>About Us</li>
						<li>Contact Us</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<h6>Contact Us  41 </h6>
					<ul>
						<li>(+21) 666 666 666</li>
						<li>Info@iptul.com</li>
						<li>Apgujeong-ro 79-gil, Gangnam-gu</li>
						<li>Seoul, South Korea</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<h6>Terms and Conditions Faq</h6>
				</div>
			</div>
		</div>
	</section>
	</body>
</html>	